﻿Import-Module SharePointPnPPowerShell2016 -Verbose

function ProcessWebsRecursively($web) {
    $subWebs = Get-PnPSubWebs -web $web
    foreach($subWeb in $subWebs) {
        ProcessWebsRecursively($subWeb)
    }

    Write-Host $web.Title
    # take some action against the $web here
}

Connect-PnpOnline -Url https://socafrica.sof.socom.mil/ -CurrentCredentials
ProcessWebsRecursively(Get-PnPWeb)