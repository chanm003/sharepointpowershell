﻿# download module from Internet
# Below command should add a "SharePointPnPPowerShell2016" onto your Desktop
Save-Module -Name "SharePointPnPPowerShell2016" -Path C:\Users\michael.chan\Desktop

# on your current machine, identify all the locations where we can "install" 3rd party modules
$env:PSModulePath -split ';'

<# "Install" simply means moving Folders around in File Explore
 Move "SharePointPnPPowerShell2016" folder to one of the folders listed in previous step
 Create folders as needed
 
 On NIPR and SIPR respectively, I move this folder to the following locations:   
 C:\Users\michael.chan\Documents\WindowsPowerShell\Modules
 \\sof\SOCEUR\michael.chan\Documents\WindowPowerShell\Modules
#>

# verify 
Import-Module SharePointPnPPowerShell2016
Get-Module SharePointPnPPowerShell2016