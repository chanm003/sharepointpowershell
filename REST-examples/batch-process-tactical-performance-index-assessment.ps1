﻿# import reusable code
Import-Module 'C:\Users\michael.chan\Desktop\PowerShell Rest API\sp-library.ps1'
cls

# variables
$subsite = "https://socafrica.sof.socom.mil/sites/POTFF"
$listName = "Tactical Performance Index Assessment"
$today = Get-Date -Format "yyyy-MM-dd"

# build query to fetch list items
$odataParams = @{}
$odataParams.Add('$top', "5000")
$listItems = readListItemsWithHttpGet -spWebUrl $subsite -listName $listName -odataParams $odataParams

# iterate over list items and update each list item if needed
foreach ($li in $listItems) {
    if ($true) { 
        updateListItem -spWebUrl $subsite -listName $listName -listItemId $li.Id -data @{"Today" = $today}
        Write-Host "Updated list item id: $($li.Id)"
        #start2013Workflow -listItemId $li.Id
    }
}