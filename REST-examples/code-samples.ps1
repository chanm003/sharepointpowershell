﻿Import-Module 'C:\Users\michael.chan\Desktop\PowerShell Rest API\sp-library.ps1'
$timestamp = Get-Date -Format "MM/dd/yyyy HH:mm:ss"
cls
<# EXAMPLE: CREATE LIST ITEM    
createListItem -spWebUrl https://socafrica.sof.socom.mil -listName PowerShellTestList  -data @{"Title" = "This list item was created from PowerShell on $timestamp"}
#>

<# EXAMPLE: UPDATE LIST ITEM 
updateListItem -spWebUrl "https://socafrica.sof.socom.mil" -listName "PowerShellTestList"  -listItemId 26 -data @{"Title" = "This item was updated by PS1 on $timestamp"}
#>

<# EXAMPLE: GET LIST ITEMS (using HTTP GET and Odata filter)
$odataParams = @{}
$odataParams.Add('$select', "Id,Title")
$odataParams.Add('$top', "5")
readListItemsWithHttpGet -spWebUrl "https://socafrica.sof.socom.mil" -listName "PowerShellTestList" 
#>

<# EXAMPLE: GET LIST ITEMS (using HTTP POST and CAML filter)
$caml = '<View><RowLimit>2</RowLimit></View>'
$odataParams = @{}
$odataParams.Add('$select', "Id,Title")
readListItemsWithHttpPost -spWebUrl "https://socafrica.sof.socom.mil" -listName "PowerShellTestList" -odataParams $odataParams -viewXml $caml
#>

<# EXAMPLE: DELETE LIST ITEM
deleteListItem -listItemId 22 -spWebUrl "https://socafrica.sof.socom.mil" -listName "PowerShellTestList"  
#>

<# EXAMPLE: SEND EMAIL  
sendEmail -spWebUrl https://socafrica.sof.socom.mil -to @('michael.chan.ctr@socom.mil') -subject "Aloha World" -body "This is the body of my first email"
#>

<# EXAMPLE: TRIGGER 2013 WORKFLOW (obtain subscription ID, go to workflow settings, then click on workflow, then look in address bar)
start2013Workflow -listItemId 26 -spWebUrl https://socafrica.sof.socom.mil -subscriptionId "11A08C95-9F9E-474F-85F5-D4FDF2CD5428"
#>
