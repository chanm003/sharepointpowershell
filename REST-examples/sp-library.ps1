﻿# Inspired by https://pholpar.wordpress.com/2017/03/29/working-with-the-rest-odata-interface-from-powershell/
# Can be used in environments where installation of 3rd party modules from PowerShell Gallery is not permitted
function buildHeadersForGetOperation {
    return @{ ‘Accept’ = ‘application/json; odata=verbose’}
}

function buildHeadersForCreateOperation {
    param (
        $spWebUrl
    )
    $digestValue = getFormDigest -spWebUrl $spWebUrl
    return @{ ‘X-RequestDigest’ = $digestValue; ‘Accept’ = ‘application/json; odata=verbose’ }
}

function buildHeadersForUpdateOperation {
    param (
        $spWebUrl
    )
    $digestValue = getFormDigest -spWebUrl $spWebUrl
    return @{ ‘X-RequestDigest’ = $digestValue; ‘Accept’ = ‘application/json; odata=verbose’; ‘IF-MATCH’ = ‘*‘; ‘X-HTTP-Method’ = ‘MERGE’ }
}

function buildHeadersForDeleteOperation {
    param (
        $spWebUrl
    )
    $digestValue = getFormDigest -spWebUrl $spWebUrl
    return @{ ‘X-RequestDigest’ = $digestValue; ‘IF-MATCH’ = ‘*’; ‘X-HTTP-Method’ = ‘DELETE’ }
}

function getFormDigest {
    param (
        $spWebUrl
    )
    $endpoint = $spWebUrl + "/_api/contextInfo"
    $resp = Invoke-RestMethod -Uri $endpoint -Method Post -UseDefaultCredentials
    return $resp.GetContextWebInformation.FormDigestValue
}

function getListEntityTypeFullName {
    param (
        $spWebUrl,
        $listName
    )

    $endpoint = [string]::Format("{0}/_api/lists/getByTitle('{1}')", $spWebUrl, $listName)
    $response = Invoke-RestMethod -uri $endpoint -UseDefaultCredentials:$true
    return $response.entry.content.properties.ListItemEntityTypeFullName
}

function convertHashTableToQueryString {
    param(
        $Data
    )
    $queryStringParts = @()
    $Data.keys | ForEach-Object {
        $queryStringParts += [string]::Format("{0}={1}", $_,$Data[$_])
    }
    return $queryStringParts -join '&'
}

# https://stackoverflow.com/questions/8800375/merging-hashtables-in-powershell-how
Function Merge-Hashtables {
    $Output = @{}
    ForEach ($Hashtable in ($Input + $Args)) {
        If ($Hashtable -is [Hashtable]) {
            ForEach ($Key in $Hashtable.Keys) {$Output.$Key = $Hashtable.$Key}
        }
    }
    $Output
}

function createListItem {
    param (
        $spWebUrl,
        $listName,
        $data
    )

    $endpoint = [string]::Format("{0}/_api/lists/getByTitle('{1}')/items", $spWebUrl, $listName)
    $listEntityTypefullName = getListEntityTypeFullName -spWebUrl $spWebUrl -listName $listName
    $basePayload = @{ ‘__metadata’ = @{ ‘type’ = $listEntityTypefullName }} 
    $payload = Merge-Hashtables $basePayload $data
    $jsonPayload = $payload | ConvertTo-Json -Depth 4

    $headers = buildHeadersForCreateOperation -spWebUrl $spWebUrl

    $result = Invoke-RestMethod -Uri $endpoint -Method Post -Body $jsonPayload -ContentType ‘application/json; odata=verbose’ -Headers $headers –UseDefaultCredentials
}

function updateListItem {
    param (
        $spWebUrl,
        $listName,
        $listItemId,
        $data
    )

    $endpoint = [string]::Format("{0}/_api/lists/getByTitle('{1}')/items({2})", $spWebUrl, $listName, $listItemId)
    $listEntityTypefullName = getListEntityTypeFullName -spWebUrl $spWebUrl -listName $listName
    $basePayload = @{ ‘__metadata’ = @{ ‘type’ = $listEntityTypefullName }} 
    $payload = Merge-Hashtables $basePayload $data
    $jsonPayload = $payload | ConvertTo-Json -Depth 4

    $headers = buildHeadersForUpdateOperation -spWebUrl $spWebUrl

    $result = Invoke-RestMethod -Uri $endpoint -Method Post -Body $jsonPayload -ContentType ‘application/json; odata=verbose’ -Headers $headers –UseDefaultCredentials
}

function deleteListItem {
    param (
        $spWebUrl,
        $listName,
        $listItemId
    )

    $endpoint = [string]::Format("{0}/_api/lists/getByTitle('{1}')/items({2})", $spWebUrl, $listName, $listItemId)
    $headers = buildHeadersForDeleteOperation -spWebUrl $spWebUrl
    $result = Invoke-RestMethod -Uri $endpoint -Method Post -ContentType ‘application/json; odata=verbose’ -Headers $headers –UseDefaultCredentials
}

function readListItemsWithHttpGet {
    param (
        $spWebUrl,
        $listName,
        $odataParams
    )

    $queryString = "";
    if($odataParams) {
        $queryString += "?"
        $queryString += convertHashTableToQueryString -Data $odataParams
    }

    $endpoint = [string]::Format("{0}/_api/lists/getByTitle('{1}')/items{2}", $spWebUrl, $listName, $queryString)
    $request = [System.Net.WebRequest]::Create($endpoint)
    $request.UseDefaultCredentials = $true
    $request.Accept = ‘application/json;odata=verbose’

    $response = $request.GetResponse()
    $reader = New-Object System.IO.StreamReader $response.GetResponseStream()
    $response = $reader.ReadToEnd() -creplace ‘"ID":’, ‘"ListItemId":’
    $result = ConvertFrom-Json -InputObject $response

    return $result.d.results
}

function readListItemsWithHttpPost {
     param (
        $spWebUrl,
        $listName,
        $odataParams,
        $viewXml
    )

    $queryString = "";
    if($odataParams) {
        $queryString += "?"
        $queryString += convertHashTableToQueryString -Data $odataParams
    }

    $endpoint = [string]::Format("{0}/_api/web/lists/getByTitle('{1}')/GetItems{2}", $spWebUrl, $listName, $queryString)
    
    if($viewXml -eq $null) {
        # default CAML
        $viewXml = '<View><RowLimit>5000</RowLimit></View>'
    }

    $headers = buildHeadersForCreateOperation -spWebUrl $spWebUrl

    $queryPayload = @{ 
        ‘query’ = @{
                ‘__metadata’ = @{ ‘type’ = ‘SP.CamlQuery’ };                      
                ‘ViewXml’ = $viewXml
        }
    } | ConvertTo-Json

    $result = Invoke-RestMethod -Uri $endpoint -Method Post -Body $queryPayload  -ContentType ‘application/json; odata=verbose’ -Headers $headers –UseDefaultCredentials    
    $resultJson = $result -creplace ‘"ID":’, ‘"ListItemId":’ | ConvertFrom-Json
    return $resultJson.d.results
}

function sendEmail {
    param (
        $spWebUrl,
        $to,
        $subject,
        $body
    )

    $endpoint = [string]::Format("{0}/_api/SP.Utilities.Utility.SendEmail", $spWebUrl)
    $payload = @{ 
        'properties' = @{
            '__metadata' = @{ ‘type’ = 'SP.Utilities.EmailProperties' };
            'From' = 'n015-portal.noreply@socom.mil';
            'To' = @{
                'results' = $to
            };
            'Body' = $body;
            'Subject' = $subject;    
        }
    } 
    $jsonPayload = $payload | ConvertTo-Json -Depth 4

    $headers = buildHeadersForCreateOperation -spWebUrl $spWebUrl

    $result = Invoke-RestMethod -Uri $endpoint -Method Post -Body $jsonPayload -ContentType ‘application/json; odata=verbose’ -Headers $headers –UseDefaultCredentials
}

function start2013Workflow {
     param (
        $spWebUrl,
        $subscriptionId,
        $listItemId
    )

    $endpoint = [string]::Format("{0}/_api/SP.WorkflowServices.WorkflowInstanceService.Current/StartWorkflowOnListItemBySubscriptionId(subscriptionId='{1}',itemId='{2}')", $spWebUrl, $subscriptionId, $listItemId)
    $headers = buildHeadersForCreateOperation -spWebUrl $spWebUrl

    $result = Invoke-RestMethod -Uri $endpoint -Method Post -ContentType ‘application/json; odata=verbose’ -Headers $headers –UseDefaultCredentials
}
