﻿Import-Module SharePointPnPPowerShell2016

$url = "https://socafrica.sof.socom.mil"
$timestamp = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
$listName = "PnpTesting"

# connect
Connect-PnpOnline -Url $url -CurrentCredentials

# create 
$createdItem = Add-PnPListItem -List $listName -Values @{"Title" = "This item was created by PnP PowerShell - $timestamp"; }
Write-Host "Created list item: $($createdItem.Id)" 

Start-Sleep -Seconds 5

# update
Set-PnPListItem -List $listName -Identity $createdItem.Id -Values @{"Title" = [string]::Format("{0}, then updated at {1}", $createdItem["Title"], (Get-Date -Format "yyyy-MM-dd HH:mm:ss"))}

# get by CAML query
$allItemsCaml = "<View></View>"
$camlQuery = "<View><Query><Where><Eq><FieldRef Name='ID' /><Value Type='Number'>8</Value></Eq></Where></Query></View>"
$camlQueryResult = Get-PnPListItem -List $listName -PageSize 5000 -Query $camlQuery
foreach($item in $camlQueryResult) {
    Write-Host $item["Title"]
}

# get by ID
$getByIdResult = Get-PnPListItem -List $listName -Id 8
Write-Host $getByIdResult["Title"]

# delete (place in Recycle Bin)
# Remove-PnPListItem -List $listName -Identity 7 -Force -Recycle