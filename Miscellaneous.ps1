﻿Import-Module SharePointPnPPowerShell2016

$url = "https://socafrica.sof.socom.mil"
$timestamp = Get-Date -Format "yyyy-MM-dd HH:mm:ss"

# connect
Connect-PnpOnline -Url $url -CurrentCredentials

Start-PnPWorkflowInstance -Subscription "SP2013Workflow" -ListItem 28

Send-PnPMail -To "michael.chan.ctr@socom.mil" -Subject "Aloha World" -Body "From PnP PowerShell"