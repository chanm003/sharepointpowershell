﻿Import-Module SharePointPnPPowerShell2016

# variables
$url = "https://socafrica.sof.socom.mil/sites/POTFF"
$listName = "Tactical Performance Index Assessment"
$today = Get-Date -Format "yyyy-MM-dd"

# connect
Connect-PnpOnline -Url $url -CurrentCredentials

# fetch all items in list
$camlQueryResult = Get-PnPListItem -List $listName -PageSize 5000
foreach($item in $camlQueryResult) {
    if ($true) { 
        Set-PnPListItem -List $listName -Identity $item.Id -Values @{"Today" = $today}
        #Start-PnPWorkflowInstance -Subscription "Send Mail" -ListItem $item.Id
    }
}