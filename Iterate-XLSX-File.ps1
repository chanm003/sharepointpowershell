﻿Import-Module ImportExcel
Import-Module SharePointPnPPowerShell2016

$url = "https://socafrica.sof.socom.mil"
$listName = "PnpTesting"

# connect
Connect-PnpOnline -Url $url -CurrentCredentials

$rowsInExcelFile = Import-Excel -Path "C:\Users\michael.chan\Desktop\PNP PowerShell\2008Sales.xlsx" 

# iterate over excel sheet rows
foreach($i in $rowsInExcelFile) {
    # create list item
    $data = @{"Title" = [string]::Format("{0} paid {1}", $i.Customer, $i.Value); } 
    $createdItem = Add-PnPListItem -List $listName -Values $data
    Write-Host "Created list item: $($createdItem.Id)" 
}